//слайдер
$(document).ready(function () {
    $(".owl-carousel--product").owlCarousel({
        loop: true,
        dots: true,
        nav: true,
        margin: 0,
        items: 1,
        navText: ['<img src="img/slider-arrow-left--black.svg" />', '<img src="img/slider-arrow-right--black.svg" />'],
        navContainer: '.owl-navs .nav',
        dotsContainer: '.owl-navs .dots',
    });
    $(".owl-carousel").owlCarousel({
        loop: true,
        dots: true,
        nav: true,
        margin: 0,
        items: 1,
        navText: ['<img src="img/slider-arrow-left.svg" />', '<img src="img/slider-arrow-right.svg" />'],
        navContainer: '.owl-navs .nav',
        dotsContainer: '.owl-navs .dots',
    });


    // $('.js-select-this').select2({
    //     minimumResultsForSearch: -1,
    //     width: '100%'
    // });


    $('.av-product__tabs-caption').on('click', 'span:not(.active)', function() {
        var index = $(this).index();

        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('section.av-product-tabs').find('div.av-product__content').removeClass('active').eq(index).addClass('active');
        $(".av-product__tabs-caption").scrollCenter(".active", 300);
    });
    $(".av-family__tabs").on("click","a", function (e) {
        e.preventDefault;
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 120}, 1500);
        // $(this).addClass('active').siblings().removeClass('active');
        // $(".av-family__tabs").scrollCenter(".active", 300);
    });

    $('.tel-mask').on('blur', function () {
        var value = $(this).val();
        if (value.search('_') != -1) {
            $(this).addClass('has-validate');
            $(this).removeClass('done');
        } else {
            $(this).removeClass('has-validate');
            $(this).addClass('done');
        }
    });

    jQuery.fn.scrollCenter = function(elem, speed) {
        var active = jQuery(this).find(elem);
        var activeWidth = active.width() / 2;
        var pos = active.position().left + activeWidth;
        var elpos = jQuery(this).scrollLeft();
        var elW = jQuery(this).width();

        pos = pos + elpos - elW / 2;
        jQuery(this).animate({
            scrollLeft: pos
        }, speed == undefined ? 1000 : speed);
        return this;
    };

    jQuery.fn.scrollCenterORI = function(elem, speed) {
        jQuery(this).animate({
            scrollLeft: jQuery(this).scrollLeft() - jQuery(this).offset().left + jQuery(elem).offset().left
        }, speed == undefined ? 1000 : speed);
        return this;
    };

    // $('.av-template__cross').on('click', function () {
    //     $(this).toggleClass('active');
    //     $(this).parents('.av-template__guarantee').toggleClass('active')
    // });
    $('.av-template__guarantee-title').on('click', function () {
        $(this).find('.av-template__cross').toggleClass('active');
        $(this).parents('.av-template__guarantee').toggleClass('active')
    });



});

function updatePhoto(e) {
    $(e).parent('.av-template__upload').addClass('active');
    $(e).siblings('.av-template__upload-loader').show();
    setTimeout(function () {
        $('.av-template__upload-loader').hide();
    }, 1000);
    $('.av-template__remove').on('click', function () {
        $(this).prev().val('');
        $(this).next().attr('src', '#');
        $(this).parent('.av-template__upload').removeClass('active');
    })
}


//отображение|скрытие мобильного меню по нажатию на "бургер"
$(document).ready(function () {
    var $hamburger = $('.hamburger');
    var _body = $('body');

    $hamburger.on('click', function (e) {
        _body.toggleClass('is_submenu');
        console.log('gg');
        if ($(window).width() > 1024) {
            $(this).toggleClass('is-active');
            $('.desktop').toggleClass('show');
        } else {
            $(this).toggleClass('is-active');

            $('.mob').toggleClass('show');
            $('.desktop').toggleClass('show');
            $('nav .col').removeClass('is_submenu');
            $('nav .col .submenu').removeClass('show');

            // if (_body.hasClass('is_submenu')) {
            //     _body.removeClass('is_submenu');
            // }

        }
    });
    $(window).resize(function () {
        $('.desktop').removeClass('show');
        $hamburger.removeClass('is-active');
        _body.removeClass('is_submenu');
        $('nav .col').removeClass('is_submenu');
        $('nav .col .submenu').removeClass('show');
        $('.mob').removeClass('show');

        // if ($(window).width() < 600) {
        //     $('.filters__box').mCustomScrollbar("destroy");
        // }
    });
});


//скрытие подменю
function hide_submenu() {
    $('nav .submenu').removeClass('show');
    $('nav .col').removeClass('is_submenu');
}

//скрытие подменю по нажатию на любую область вне "шапки" сайта
$(document).ready(function () {
    $('html, body').on('click', function () {
        var _body = $('body');
        var eventInMenu = $(event.target).parents('header');

        if (!eventInMenu.length) {
            hide_submenu();
            $('header .mob').removeClass('show');
            _body.removeClass('is_submenu');
            $(".hamburger").removeClass("is-active");
        }
    })
});

//отображение|скрытие подменю по нажатию на стрелочку в "шапке" сайта
$(document).ready(function () {
    $('nav .more').on('click', function (e) {
        var _body = $('body');
        var _this = $(this);
        var _parent = _this.parent();
        var _parent_isShow = _parent.hasClass('is_submenu');
        var _submenu = _parent.find('.submenu');
        var _submenu_isShow = _submenu.hasClass('show');

        $('nav .col').removeClass('is_submenu');
        $('nav .submenu').removeClass('show');
        // _body.removeClass('is_submenu');

        if (_parent_isShow && _submenu_isShow) {
            _parent.removeClass('is_submenu');
            // _body.removeClass('is_submenu');
            _submenu.removeClass('show');
        } else {
            // _body.addClass('is_submenu');
            _parent.addClass('is_submenu');
            _submenu.addClass('show');
        };
    });
});

// Выбор города

function townSelect() {
    $(document).on('click', '.js-town', function (e) {
        e.preventDefault();
        var town = $(this);
        $('.js-town').removeClass('active');
        town.addClass('active');
    });
    $(document).on('click', '.js-close-selector', function () {
        var closer = $(this);
        var container = closer.closest('.selector');
        container.removeClass('active');
    });
    $(document).on('click', '.selector__link', function (e) {
        e.preventDefault();
        var s_link = $(this);
        var container = s_link.parents('.selector');
        if (container.hasClass('active')) {
            container.removeClass('active');
        } else {
            container.addClass('active');
        }
    });
    $(document).on('click', '.js-checks-close', function () {
        var button = $(this);
        var ch_box = button.closest('.selector__checkers');
        if (button.hasClass('active')) {
            ch_box.removeClass('active');
            button.removeClass('active');
        } else {
            ch_box.addClass('active');
            button.addClass('active');
        }
    });
}

function closeFounded() {
    $(document).on('click', '.js-founded', function (e) {
        e.stopPropagation();
        for (var k = 0; k < partners_objects.length; k++) {
            var pointer = partners_objects[k];
            pointer.options.set({ iconImageHref: 'img/map-point.png'});
        }
        var button = $(this);
        var box = button.closest('.founded');
        box.removeClass('active');
    })
}

function sorterButtons() {
    $(document).on('click', '.js-sort-tags', function () {
        var btn = $(this);
        var field = $('.tags__tags');
        if (btn.hasClass('active')) {
            btn.removeClass('active');
            field.removeClass('active');
        } else {
            $('.js-sort-town').removeClass('active');
            $('.tags').removeClass('active');
            btn.addClass('active');
            field.addClass('active');

        }
    });

    $(document).on('click', '.js-sort-town', function () {
        var btn = $(this);
        var field = $('.tags__towns');
        if (btn.hasClass('active')) {
            btn.removeClass('active');
            field.removeClass('active');
        } else {
            $('.js-sort-tags').removeClass('active');
            $('.tags').removeClass('active');
            btn.addClass('active');
            field.addClass('active');

        }
    });

    $(document).on('click', '.js-reset-tags', function () {
        var btn = $(this);
        console.log('click');
        var container = $('.tags__checkboxes');
        $('.js-reset-tags').removeClass('active');
        container.find('.tag input').prop('checked', false);
    });

    $(".sorter__box").on("click","a", function (e) {
        e.preventDefault;
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 120}, 1000);
        // $(this).addClass('active').siblings().removeClass('active');
        // $(".av-family__tabs").scrollCenter(".active", 300);
    });
}


function showMorePeople(){
    $(document).on('click', '.js-show-more-people', function (e) {
        e.preventDefault();
        $('.people').find('.man').addClass('active');
    });
}

function showTagsReser() {
    $(document).on('click', '.tag', function () {
        $('.tag input').each(function () {
            if ($(this).prop('checked') === true) {
                $('.js-reset-tags').addClass('active');
                return false;
            } else {
                $('.js-reset-tags').removeClass('active');
            }
        })
    })
}

//отображение|скрытие разделов и их адаптивность, где есть отображение по нажатию, в блоках "привелегии, преимущества"
$(document).ready(function () {
    showTagsReser();
    showMorePeople();
    sorterButtons();
    closeFounded();
    townSelect();
    var _toggle = $('[data-toggle]');

    // $('.filters').each(element, new SimpleBar);
    $(".filters__box").mCustomScrollbar();
    $(".js-scroll-this").mCustomScrollbar();

    _toggle.click(function () {
        var _this = $(this);
        var _target = $(_this.data('toggle'));
        var _targetMob = _this.find('.list__item-mob');
        var _isTargetActive = _target.hasClass('active');
        var _root = _this.parent('.list__item');
        var _childs = _root.find('.list__item-item');
        var _targetMobs = _root.find('.list__item-mob');

        _targetMobs.removeClass('active');
        _childs.removeClass('active');
        _childs.find('.toggle_target').removeClass('active');

        if (_isTargetActive) {
            _targetMob.removeClass('active');
            _this.removeClass('active');
            _target.removeClass('active');
        } else {
            var _parentOffsetLeft = -((_this.position().left - _this.width() > 0) ? _this.width() + 15 * 3 + 1 : 15);

            _targetMob.addClass('active').css({
                width: _root.width(),
                left: _parentOffsetLeft
            });
            _this.addClass('active');
            _target.addClass('active');
        }

        $(window).resize(function () {
            var _parent = _targetMob.parent();
            var _root = _parent.parent();

            var _parentOffsetLeft = -((_parent.position().left - _parent.width() > 0) ? _parent.width() + 15 * 3 + 1 : 15);

            _targetMob.css({
                width: _root.width(),
                left: _parentOffsetLeft
            });
        })
    })



});

//фиксация меню при скроле
$(window).scroll(function () {
    var header = $('header'),
        scroll = $(window).scrollTop(),
        top_shift = header.outerHeight(),
        _isSubMenu = $('.is_submenu').length;


    if (scroll >= top_shift) {
        if (!header.hasClass('fixed')) {

            header.addClass('fixed');
        }
    } else {
        header.removeClass('fixed');
    }

    if ($('header .mob').hasClass('show') && !_isSubMenu) {
        $('header .mob').removeClass('show');
        $(".hamburger").removeClass("is-active");
        $('.desktop').removeClass('show');
    }
});

//скрытие подменю при скроле
$(window).scroll(function () {
    var _isSubMenuClose = false;
    var _subMenu = $('.is_submenu');
    var _scrollTop = $('.is_submenu');

    _isSubMenuClose = _subMenu.length;

    if (window.innerWidth > 1025 || !_isSubMenuClose) {
        hide_submenu();
    }
});


function paintSearch(field) {
    var container = $('.filters');
    var main_string = field.val();
    var lower_main_string = main_string.toLowerCase();
    var length = main_string.length + 1;
    var item = container.find('.founded');
    item.each(function () {
        var string = $(this).find('.founded__header').attr('data-text');
        var lower_string = $(this).find('.founded__header').attr('data-text').toLowerCase();
        var ar_string = string.split('');
        var start = lower_string.indexOf(lower_main_string);

        if (lower_string.includes(lower_main_string)) {
            ar_string.splice(start, 0, '<span class="blue">');
            ar_string.splice(start + length, 0, '</span>' );
            $(this).find('.founded__header').html(string);
            $(this).find('.founded__header').html(ar_string.join(''));
            $(this).closest('.founded').addClass('display');
        } else {
            $(this).closest('.founded').removeClass('display');
        }
    });
    if (main_string.length > 0) {
        $('.js-clear-search').addClass('cross');
    } else {
        $('.js-clear-search').removeClass('cross');
    }
}

function clearSearchField() {
    $(document).on('click', '.js-clear-search.cross', function () {
        var field = $('.js-paint-search');
        field.val('');
        $(this).removeClass('cross');
        paintSearch(field);
    })
}

function highlightSearchResult() {
    $(document).on('click', '.founded', function(){
        var item = $(this);
        if (item.hasClass('active')) {
            return false;
        } else {
            $('.founded').removeClass('active');
            item.addClass('active');
        }
    })
}

//валидация форм
$(document).ready(function () {
    $('.js-paint-search').on('keyup', function () {
        paintSearch($(this));
    });
    clearSearchField();
    highlightSearchResult();
    uirange();
    Maskedinput();
    $('form').validator()
        .on('submit', function (e) {

            var _this = $(this);

            if (e.isDefaultPrevented()) {

                _this.addClass('was-validate');

                console.log('invalid');

            } else {

                _this.removeClass('was-validate');

                e.preventDefault();
            }
        }).on('invalid.bs.validator', function (e) {
            $(e.relatedTarget).parents('.input_field').addClass('has-validate');
            $(e.relatedTarget).parents('.av-template__upload').addClass('has-validate');
            $(e.relatedTarget).addClass('has-validate');
            $(e.relatedTarget).addClass('validate');
        }).on('valid.bs.validator', function (e) {
            $(e.relatedTarget).parents('.input_field').removeClass('has-validate');
            $(e.relatedTarget).parents('.av-template__upload').removeClass('has-validate');
            $(e.relatedTarget).removeClass('has-validate');

        });

});

function Maskedinput(){
    if($('.tel-mask')){
        $('.tel-mask').mask('+7(999)999-99-99', {placeholder:"_", autoclear: false});
    }
}
function uirange() {
    var elems = $( '.js-slider-range' );
    if(elems.length){
        elems.each(function(){
            var slider = $(this);
            var min = slider.closest('.av-config__range').find('.js-slider-range-min');
            var max = slider.closest('.av-config__range').find('.js-slider-range-max');
            slider.slider({
                range: true,
                min: slider.data('min') ? slider.data('min') : 0,
                max: slider.data('max') ? slider.data('max') : 500,
                values: slider.data('current') ? slider.data('current') : [12, 100],
                slide: function( event, ui ) {
                    min.val(ui.values[ 0 ]);
                    max.val(ui.values[ 1 ]);
                }
            });
            min.val(slider.slider( "values", 0 ));
            max.val(slider.slider( "values", 1 ));
            setInputFilter(min[0], function(value) {
                return /^\d*\.?\d*$/.test(value);
            });
        });
    }

};
function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.addEventListener(event, function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    });
}
